package elysium.common;

import elysium.common.lib.CreativeTabElysium;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * Created by dawar on 2016. 02. 01..
 */

@Mod(
        modid = Elysium.MODID,
        name = Elysium.MODNAME,
        version = Elysium.VERSION,
        guiFactory = "elysium.client.ElysiumGuiFactory", dependencies = "required-after:Forge@[12.16.1.1887,)"
)
public class Elysium {
    public static final String MODID = "elysium";
    public static final String MODNAME = "Elysium";
    public static final String VERSION = "2.0.0";
    @SidedProxy(
            clientSide = "elysium.client.ClientProxy",
            serverSide = "elysium.common.CommonProxy"
    )
    public static CommonProxy proxy;
    @Instance(Elysium.MODID)
    public static Elysium instance;

    public File modDir;
    public static final Logger log = LogManager.getLogger("ELYSIUM");

    public static CreativeTabs tabElysium = new CreativeTabElysium(CreativeTabs.getNextID(), "elysium");

    public Elysium() {
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        event.getModMetadata().version = Elysium.VERSION;
        this.modDir = event.getModConfigurationDirectory();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent evt) {

     }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent evt) {


    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
//        if (eventArgs.getModID().equals(Elysium.MODID)) {
//            Config.syncConfigurable();
//            if (Config.config != null && Config.config.hasChanged()) {
//                Config.save();
//            }
//        }

    }
}
